import java.awt.*;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

public abstract class Actor {
    Color colour;
    Cell loc;
    List<Polygon> actors = new ArrayList<>();

    public void paint(Graphics g){
        for(int i = 0; i < actors.size(); i++){
            g.setColor(Color.BLACK);
            g.drawPolygon(actors.get(i));
            g.setColor(colour);
            g.fillPolygon(actors.get(i));
        }
    }
}