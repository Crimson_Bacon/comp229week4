import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Stage {
    Grid grid;
    Actor puppy;
    Actor lion;
    Actor rabbit;

    public Stage(){
        grid = new Grid();
        puppy = new Puppy(grid.cellAtColRow(0, 0));
        lion = new Lion(grid.cellAtColRow(0, 18));
        rabbit = new Rabbit(grid.cellAtColRow(14,3));
    }

    public void paint(Graphics g, Point mouseLoc){
        grid.paint(g,mouseLoc);
        List<Actor> actors = new ArrayList<>();
        actors.add(puppy);
        actors.add(lion);
        actors.add(rabbit);
        for(int i = 0; i < actors.size(); i++){
            actors.get(i).paint(g);
        }
    }
}